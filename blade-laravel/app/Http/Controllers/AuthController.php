<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function welcome(Request $request)
    {
        $nama_depan = $request -> input('nama_depan');
        $nama_belakang = $request -> input('nama_belakang');

        return view('index', compact('nama_depan', 'nama_belakang'));
    }

    public function register()
    {
        return view('form');
    }
}
