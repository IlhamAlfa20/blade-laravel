<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function home()
    {
        return view('home');
    }

    public function table() 
    {
        return view('table');
    }

    public function data_tables()
    {
        return view('data-tables');
    }
}
