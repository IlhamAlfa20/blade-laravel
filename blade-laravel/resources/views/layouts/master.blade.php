<html lang="en">

<head>

    @include('layouts.partials.header')
    @yield('stylesheets')

</head>

<body>
    <div id="wrapper">
        <div class="sidebar">
            @include('layouts.partials.l-sidebar')

            <div id="page-wrapper" class="gray-bg dashbard-1">
                <div class="navbar-nav ml-auto">
                    @include('layouts.partials.navbar')
                </div>

                <div class="content-wrapper">
                    <div class="content-header">
                        @yield('content')
                    </div>
                </div>

                @include('layouts.partials.footer')
            </div>
        </div>
    </div>

    @include('layouts.partials.scripts')
    @yield('scripts')

</body>

</html>
