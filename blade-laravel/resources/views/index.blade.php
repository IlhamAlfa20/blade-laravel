@extends('layouts.master')

@section('title', config('app.name').'Dashboard')

@section('stylesheets')
<meta name="csrf-token" content="{{ csrf_token() }}">

<link rel="stylesheet" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">
<style>
    th {
        font-size: 13px;
        text-align: center;
    }
    td {
        font-size: 13px;
    }
</style>
@endsection

@section('scripts')

@endsection

@section('content')
<h1>SELAMAT DATANG! {{$nama_depan}} {{$nama_belakang}}</h1><br>
<h2>Terima Kasih Telah Bergabung di SanberBook. Social Media Kita Bersama</h2>
@endsection