@extends('layouts.master')

@section('title', config('app.name').' | Dashboard')

@section('stylesheets')
<meta name="csrf-token" content="{{ csrf_token() }}">

<link rel="stylesheet" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">
<style>
    th {
        font-size: 13px;
        text-align: center;
    }
    td {
        font-size: 13px;
    }
</style>
@endsection

@section('scripts')

@endsection

@section('content')
<h1>Buat Account Baru</h1>
<h3>Sign Up Form</h3>

<form method="POST" action="{{ url('index') }}">
{{@csrf_field()}}
    <label>First Name :</label><br><br>
    <input type="text" name="nama_depan"><br><br>

    <label>Last Name :</label><br><br>
    <input type="text" name="nama_belakang"><br><br>

    <label>Gender :</label><br><br>
    <input type="radio" id="male" name="gender" value="male">
    <label for="male">Male</label><br>
    <input type="radio" id="female" name="gender" value="female">
    <label for="female">Female</label><br>
    <input type="radio" id="other" name="gender" value="other">
    <label for="other">Other</label>

    <label>Nationality</label><br><br>
    <select>
        <option value="id">Indonesia</option>
        <option value="sgp">Singaporean</option>
        <option value="mly">Malaysian</option>
        <option value="aus">Autralian</option>
    </select>
    <br><br>

    <label>Language Spoken :</label><br><br>
    <input type="checkbox">
    <label> Bahasa Indonesia</label><br>
    <input type="checkbox">
    <label> English</label><br>
    <input type="checkbox">
    <label> Other Language</label><br><br>

    <label>Bio :</label><br><br>
    <textarea rows="4" cols="50"></textarea><br><br>

    <button type="submit">Sign Up</button>

</form>
@endsection
