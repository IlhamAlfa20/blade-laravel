@extends('layouts.master')

@section('title', config('app.name').' | Dashboard')

@section('stylesheets')
<meta name="csrf-token" content="{{ csrf_token() }}">

<link rel="stylesheet" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">
<style>
    th {
        font-size: 13px;
        text-align: center;
    }
    td {
        font-size: 13px;
    }
</style>
@endsection

@section('scripts')

@endsection

@section('content')
    <h1>SanberBook</h1>

    <h2>Social Media Developer Santai Berkualitas</h2>

    <p>Belajar dan Berbagi agar hidup ini semakin santai Berkualitas</p>

    <h3>Benefit Join SanberBook</h3>

    <ul>
        <li>Mendapat Motivasi dari sesama Developer</li>
        <li>Sharing Knowledge dari Para Mastar Sanber</li>
        <li>Dibuat Oleh Calon Web Developer Terbaik</li>
    </ul>

    <h3>Cara Bergabung ke SanberBook</h3>

    <ol>
        <li>Mengunjungi Website ini</li>
        <li>Mendaftar di Form<a href="{{ url('/form') }}"> Form Sign Up</a></li>
        <li>Selesai !</li>
    </ol>
@endsection

